// ==UserScript==
// @name            HelpUs
// @author          Anthony Schmieder
// @version         0.1
// @description     This script makes MarkUs faster and easier to use
// @namespace       https://cs.uwaterloo.ca/~aschmied
// @grant           none
// @include         https://markus*.student.cs.uwaterloo.ca/*
// ==/UserScript==

(function() {

  // the annotation category button id needs to be updated for each assignment
  // you can get them easily using the Inspector in the Firefox web console:
  // click the "select element with mouse" button (on the left. it looks like a 
  // tv screen with an arrow pointing to it) and mouse over each of the category
  // buttons (Contract, etc) to get the new id.
  // This could be automated by looking for <li>s with innerText equal to the
  // button labels.
  var altBindings = {
    'U': function() {code_tab_menu.setActiveTab('code_holder'); document.getElementsByClassName("code_scroller")[0].focus();},
    'A': function() {code_tab_menu.setActiveTab('annotations_summary');},
    'N': function() {document.getElementById("new_annotation_button").click();},
    'C': function() {document.getElementById("annotation_category_110").click();},
    'P': function() {document.getElementById("annotation_category_111").click();},
    'E': function() {document.getElementById("annotation_category_112").click();},
    'T': function() {document.getElementById("annotation_category_113").click();},
    'H': function() {document.getElementById("annotation_category_114").click();},
    'L': function() {document.getElementById("annotation_category_115").click();},
    'M': function() {document.getElementById("annotation_category_116").click();},
    'W': function() {document.getElementById("annotation_category_117").click();}
  };

// keyboard shortcut code code based on SO answer: http://stackoverflow.com/questions/5267288/javascript-to-capture-shortcut-and-run-some-action
document.onkeydown = function(e) {
  if(e.altKey) {
    var key = String.fromCharCode(e.which);
    var boundAction = altBindings[key];
    if(boundAction) {
      boundAction();
      return false;
    }
  }
  return true;
}

})();
